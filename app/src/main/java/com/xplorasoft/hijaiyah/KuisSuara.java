package com.xplorasoft.hijaiyah;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class KuisSuara extends AppCompatActivity implements View.OnClickListener {

    int[] suaraku = {R.raw.h1,R.raw.h6,R.raw.h13,R.raw.h4,R.raw.h22,
            R.raw.h9,R.raw.h26,R.raw.h17,R.raw.h2,R.raw.h15};

    int[] opsiA = {R.drawable.h1,R.drawable.h2,R.drawable.h27,R.drawable.h11,R.drawable.h7,R.drawable.h2,R.drawable.h26,R.drawable.h26,R.drawable.h9,R.drawable.h14};
    int[] opsiB = {R.drawable.h5,R.drawable.h4,R.drawable.h7,R.drawable.h4,R.drawable.h24,R.drawable.h9,R.drawable.h6,R.drawable.h27,R.drawable.h2,R.drawable.h22};
    int[] opsiC = {R.drawable.h12,R.drawable.h3,R.drawable.h29,R.drawable.h10,R.drawable.h22,R.drawable.h3,R.drawable.h16,R.drawable.h8,R.drawable.h4,R.drawable.h15};
    int[] opsiD = {R.drawable.h17,R.drawable.h6,R.drawable.h13,R.drawable.h30,R.drawable.h6,R.drawable.h18,R.drawable.h30,R.drawable.h17,R.drawable.h10,R.drawable.h19};

    String[] kunci = {"a","d","d","b","c","b","a","d","b","c"};

    int soal = 0;
    int jumlahBenar = 0;

    RelativeLayout suara1;
    ImageView suara2;
    CardView opsia,opsib,opsic,opsid;
    ImageView imgA,imgB,imgC,imgD;
    TextView soalKe;

    MediaPlayer h30;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_kuis_suara);

        soalKe = findViewById(R.id.soalKe);

        suara1 = findViewById(R.id.suara);
        suara1.setOnClickListener(this);
        suara2 = findViewById(R.id.suara2);
        suara2.setOnClickListener(this);

        opsia = findViewById(R.id.opsia);
        opsia.setOnClickListener(this);
        opsib = findViewById(R.id.opsib);
        opsib.setOnClickListener(this);
        opsic = findViewById(R.id.opsic);
        opsic.setOnClickListener(this);
        opsid = findViewById(R.id.opsid);
        opsid.setOnClickListener(this);

        imgA = findViewById(R.id.imgA);
        imgB = findViewById(R.id.imgB);
        imgC = findViewById(R.id.imgC);
        imgD = findViewById(R.id.imgD);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        setSoal();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.suara:
                h30 = MediaPlayer.create(KuisSuara.this, suaraku[soal]);
                h30.setVolume(1,1);
                h30.start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        h30.release();
                    }
                },1000);
                break;

            case R.id.opsia:
                cekJawaban("a");
                break;

            case R.id.opsib:
                cekJawaban("b");
                break;

            case R.id.opsic:
                cekJawaban("c");
                break;

            case R.id.opsid:
                cekJawaban("d");
                break;
        }
    }

    public void cekJawaban(String jawaban)
    {
        if(jawaban.equals(""+kunci[soal]))
        {
            jumlahBenar = jumlahBenar + 1;
            //Tampilkan dialog benar
            dialog_benar();
        }
        else
        {
            //Tampilkan dialog salah
            dialog_salah();
        }
    }

    public void setSoal()
    {
        if(soal < 10)
        {
            imgA.setImageResource(opsiA[soal]);
            imgB.setImageResource(opsiB[soal]);
            imgC.setImageResource(opsiC[soal]);
            imgD.setImageResource(opsiD[soal]);

            int soalkeint = soal+1;
            soalKe.setText("Soal "+soalkeint);
        }
        else
        {
            //Dialog Kuis Selesai, tamnpilkan skor
            Intent intent = new Intent(KuisSuara.this,Skor.class);
            intent.putExtra("benar",jumlahBenar);
            startActivity(intent);
        }
    }


    private void dialog_salah() {

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_salah);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        ImageButton tombol = (ImageButton) dialog.findViewById(R.id.tombol);
        tombol.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                soal = soal+1;
                setSoal();
            }
        });
        LinearLayout layoutku = (LinearLayout) dialog.findViewById(R.id.layoutku);
        layoutku.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                soal = soal+1;
                setSoal();
            }
        });
        final MediaPlayer media = MediaPlayer.create(KuisSuara.this, R.raw.incorrect);
        media.setVolume(1,1);
        media.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                media.release();
            }
        },1000);
        dialog.show();
    }

    private void dialog_benar() {

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_benar);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        ImageButton tombol = (ImageButton) dialog.findViewById(R.id.tombol);
        tombol.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                soal = soal + 1;
                setSoal();
            }
        });
        LinearLayout layoutku = (LinearLayout) dialog.findViewById(R.id.layoutku);
        layoutku.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                soal = soal+1;
                setSoal();
            }
        });
        final MediaPlayer media = MediaPlayer.create(KuisSuara.this, R.raw.correct);
        media.setVolume(1,1);
        media.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                media.release();
            }
        },1000);
        dialog.show();
    }

}
