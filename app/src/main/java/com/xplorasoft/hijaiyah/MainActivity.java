package com.xplorasoft.hijaiyah;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.image);

        MobileAds.initialize(this, ""+getString(R.string.admob_app_id));

        playAnimation();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                transition();

            }
        },2000);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    public void playAnimation()
    {
        YoYo.with(Techniques.Bounce)
                .duration(2000)
                .playOn(imageView);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.Bounce)
                        .duration(1000)
                        .playOn(imageView);
            }
        },1000);*/
    }

    public void transition()
    {
        Intent splash2 = new Intent(MainActivity.this, Splash.class);
        startActivity(splash2);
        Animatoo.animateFade(this);
    }
}
