package com.xplorasoft.hijaiyah;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class BelajarDetail extends AppCompatActivity {

    private AdView mAdView;
    ImageView back,img, nina;
    TextView teks;
    Button suara;
    MediaPlayer h30;
    Handler handler;

    String getId = "0";
    int idSelect = 0;

    String[] textHuruf = {"Alif","Ba","Ta","Tsa","Jim","Kha","Kho","Dal","Dzal","Ra","Za","Sin","Syin","Shod","Dhod","Tho","Dhlo",
                          "'ain","Ghoin","Fa","Qof","Kaf","Lam","Mim","Nun","Wawu","Ha","Lam Alif","Hamzah","Ya"};
    int[] gambarHuruf = {R.drawable.h1,R.drawable.h2,R.drawable.h3,R.drawable.h4,R.drawable.h5,
                         R.drawable.h6,R.drawable.h7,R.drawable.h8,R.drawable.h9,R.drawable.h10,
                         R.drawable.h11,R.drawable.h12,R.drawable.h13,R.drawable.h14,R.drawable.h15,
                         R.drawable.h16,R.drawable.h17,R.drawable.h18,R.drawable.h19,R.drawable.h20,
                         R.drawable.h21,R.drawable.h22,R.drawable.h23,R.drawable.h24,R.drawable.h25,
                         R.drawable.h26,R.drawable.h27,R.drawable.h28,R.drawable.h29,R.drawable.h30};

    int[] suaraku = {R.raw.h1,R.raw.h2,R.raw.h3,R.raw.h4,R.raw.h5,
            R.raw.h6,R.raw.h7,R.raw.h8,R.raw.h9,R.raw.h10,
            R.raw.h11,R.raw.h12,R.raw.h13,R.raw.h14,R.raw.h15,
            R.raw.h16,R.raw.h17,R.raw.h18,R.raw.h19,R.raw.h20,
            R.raw.h21,R.raw.h22,R.raw.h23,R.raw.h24,R.raw.h25,
            R.raw.h26,R.raw.h27,R.raw.h28,R.raw.h29,R.raw.h30};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_belajar_detail);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        img = findViewById(R.id.img);
        nina = findViewById(R.id.nina);
        teks = findViewById(R.id.text);
        suara = findViewById(R.id.suara);

        try {
            getId = ""+getIntent().getStringExtra("huruf");
            idSelect = Integer.parseInt(""+getId);
            img.setImageResource(gambarHuruf[idSelect]);
            teks.setText(textHuruf[idSelect]);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        suara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                h30 = MediaPlayer.create(BelajarDetail.this, suaraku[idSelect]);
                h30.setVolume(1,1);
                h30.start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        h30.release();
                    }
                },1000);
                YoYo.with(Techniques.Flash)
                        .duration(2000)
                        .playOn(teks);
                playAnimation();
                YoYo.with(Techniques.RubberBand)
                        .duration(2000)
                        .playOn(img);
                playAnimation();
            }
        });

        playAnimation();
    }

    public void playAnimation()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.Bounce)
                        .duration(2000)
                        .playOn(nina);
                playAnimation();
            }
        },3000);
    }
}
