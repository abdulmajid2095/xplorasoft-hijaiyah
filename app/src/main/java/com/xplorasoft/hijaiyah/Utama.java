package com.xplorasoft.hijaiyah;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Utama extends AppCompatActivity {

    private AdView mAdView;

    Button belajar, kuis, tentang,  keluar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_utama);

        belajar  =  findViewById(R.id.belajar);
        kuis  =  findViewById(R.id.kuis);
        tentang = findViewById(R.id.tentang);
        keluar = findViewById(R.id.keluar);

        belajar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(Utama.this,Belajar.class);
                startActivity(intent);
                //Animatoo.animateSwipeLeft(Utama.this);
            }
        });

        kuis. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"Fitur dalam pengembangan",Toast.LENGTH_SHORT).show();
                Intent intent =  new Intent(Utama.this,Kuis.class);
                startActivity(intent);
            }
        });

        tentang. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"Fitur dalam pengembangan",Toast.LENGTH_SHORT).show();
                Intent intent =  new Intent(Utama.this,Tentang.class);
                startActivity(intent);
            }
        });


        keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mAdView = findViewById(R.id.adView);
        //AdRequest adRequest = new AdRequest.Builder().build();
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest adRequest = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .tagForChildDirectedTreatment(true)
                .setIsDesignedForFamilies(true)
                .build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        dialogkeluar();
    }

    private void dialogkeluar() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_keluar);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button ya = dialog.findViewById(R.id.ya);
        ya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent keluar = new Intent(Utama.this, MainActivity.class);
                startActivity(keluar);
                finish();
                moveTaskToBack(true);

            }
        });

        Button tidak = dialog.findViewById(R.id.tidak);
        tidak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
