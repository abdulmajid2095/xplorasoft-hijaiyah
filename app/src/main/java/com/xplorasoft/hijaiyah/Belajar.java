package com.xplorasoft.hijaiyah;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Belajar extends AppCompatActivity {

    private AdView mAdView;

    ImageView back;
    ListView listView;
    Adapter_Huruf adapter_huruf = null;

    String[] posisi = {"0","2","4","6","8","10","12","14","16","18","20","22","24","26","28"};
    String[] posisi2 = {"1","3","5","7","9","11","13","15","17","19","21","23","25","27","29"};
    int[] hurufA = {R.drawable.h1,R.drawable.h3,R.drawable.h5,R.drawable.h7,R.drawable.h9,R.drawable.h11,R.drawable.h13,R.drawable.h15,R.drawable.h17,R.drawable.h19,R.drawable.h21,R.drawable.h23,R.drawable.h25,R.drawable.h27,R.drawable.h29};
    int[] hurufB = {R.drawable.h2,R.drawable.h4,R.drawable.h6,R.drawable.h8,R.drawable.h10,R.drawable.h12,R.drawable.h14,R.drawable.h16,R.drawable.h18,R.drawable.h20,R.drawable.h22,R.drawable.h24,R.drawable.h26,R.drawable.h28,R.drawable.h30};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_belajar);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        listView = findViewById(R.id.list);
        adapter_huruf = new Adapter_Huruf(Belajar.this, posisi);
        listView.setAdapter(adapter_huruf);
        adapter_huruf.notifyDataSetChanged();

        mAdView = findViewById(R.id.adView);
        //AdRequest adRequest = new AdRequest.Builder().build();
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest adRequest = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .tagForChildDirectedTreatment(true)
                .setIsDesignedForFamilies(true)
                .build();
        mAdView.loadAd(adRequest);
    }






    public class Adapter_Huruf extends ArrayAdapter<String> {

        Context c;
        LayoutInflater inflater;
        public Adapter_Huruf(Context context, String[] position) {
            super(context, R.layout.model_list,position);

            this.c = context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            //coba
            //View view = null;

            if (convertView == null)
            {
                LayoutInflater inflater = getLayoutInflater();
                convertView = inflater.inflate(R.layout.model_list, parent, false);
            }

            ImageView imageA = convertView.findViewById(R.id.img1);
            ImageView imageA2 = convertView.findViewById(R.id.img1b);
            ImageView imageB = convertView.findViewById(R.id.img2);
            ImageView imageB2 = convertView.findViewById(R.id.img2b);
            imageA.setImageResource(hurufA[position]);
            imageB.setImageResource(hurufB[position]);

            imageA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Belajar.this,BelajarDetail.class);
                    intent.putExtra("huruf",""+posisi[position]);
                    startActivity(intent);
                    //Animatoo.animateZoom(Belajar.this);
                }
            });
            imageA2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Belajar.this,BelajarDetail.class);
                    intent.putExtra("huruf",""+posisi[position]);
                    startActivity(intent);
                    //Animatoo.animateZoom(Belajar.this);
                }
            });

            imageB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Belajar.this,BelajarDetail.class);
                    intent.putExtra("huruf",""+posisi2[position]);
                    startActivity(intent);
                    //Animatoo.animateZoom(Belajar.this);
                }
            });
            imageB2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Belajar.this,BelajarDetail.class);
                    intent.putExtra("huruf",""+posisi2[position]);
                    startActivity(intent);
                    //Animatoo.animateZoom(Belajar.this);
                }
            });
            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Animatoo.animateSwipeRight(Belajar.this);
    }
}
